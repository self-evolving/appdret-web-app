module.exports = {
	verbose: true,
	preset: 'ts-jest',
	testEnvironment: 'jsdom',
	resetMocks: true,
	clearMocks: true,
	setupFilesAfterEnv: ['@testing-library/jest-dom'],
	transform: {
		'^.+\\.(ts|tsx|js|jsx)$': 'ts-jest',
		'.+\\.(css|scss|png|jpg|svg)$': 'jest-transform-stub'
	},

	moduleNameMapper: {
		'/.(css|less|scss|sass)$/': 'identity-obj-proxy'
	},
	resolver: undefined,
	testMatch: ['**/?(*.)+(spec|test).(ts|tsx)'],
	collectCoverage: true,
	collectCoverageFrom: [
		'src/**/*.{ts,tsx}',
		'!src/**/*.{types,stories,constants,test,spec}.{ts,tsx}',
		'!src/App.tsx',
		'!src/index.{ts,tsx,html}',
		'!src/Types/**'
	],
	reporters: [
		'default',
		[
			'jest-junit',
			{
				outputDirectory: 'coverage/reports',
				outputName: 'junit.xml'
			}
		]
	]
};
