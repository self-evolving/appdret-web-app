FROM --platform=linux/amd64 node:latest AS build-deps
EXPOSE 80
WORKDIR /app

COPY package*.json yarn.lock ./

RUN yarn

COPY . .

RUN yarn build

FROM nginx:mainline-alpine3.18-slim
COPY --from=build-deps /app/build /usr/share/nginx/html

CMD ["nginx", "-g", "daemon off;"]
