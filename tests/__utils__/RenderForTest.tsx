import {render, RenderResult} from '@testing-library/react';
import {BrowserRouter as Router} from 'react-router-dom';

export function renderForTest(element: React.ReactElement): RenderResult {
	return render(<Router>{element}</Router>);
}
