import * as QuestionService from '../../src/Services/QuestionService';

export const mockedUseNavigate = jest.fn();
export const mockedGetQuestions = jest.fn();
export const mockedUseLocation = jest.fn().mockReturnValue({
	state: {
		pointsScored: 8,
		numberOfQuestions: 10
	}
});

jest.mock('react-router-dom', () => ({
	...jest.requireActual('react-router-dom'),
	useLocation: () => mockedUseLocation,
	useNavigate: () => mockedUseNavigate
}));

jest.mock('../../src/Services/QuestionService', () => ({
	...jest.requireActual<typeof QuestionService>(
		'../../src/Services/QuestionService'
	),
	getQuestions: mockedGetQuestions
}));
