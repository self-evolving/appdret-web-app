import {Question} from '../../src/Entities/Question';

export const QUESTION_GENERAL_BASIC_1: Question = {
	id: 1,
	advancement: 'BASIC',
	category: 'General',
	text: 'Question general basic 1',
	answers: [
		{
			text: 'Q general basic 1 answer 1 false',
			correct: false,
			explanation: 'Q general basic 1 explanation 1'
		},
		{
			text: 'Q general basic 1 answer 2 false',
			correct: false,
			explanation: 'Q general basic 1 explanation 2'
		},
		{
			text: 'Q general basic 1 answer 3 true',
			correct: true,
			explanation: 'Q general basic 1 explanation 3'
		},
		{
			text: 'Q general basic 1 answer 4 false',
			correct: false,
			explanation: 'Q general basic 1 explanation 4'
		}
	]
};

export const QUESTION_GENERAL_BASIC_2: Question = {
	id: 2,
	advancement: 'BASIC',
	category: 'General',
	text: 'Question general basic 2',
	answers: [
		{
			text: 'Q general basic 2 answer 1 false',
			correct: false,
			explanation: 'Q general basic 2 explanation 1'
		},
		{
			text: 'Q general basic 2 answer 2 false',
			correct: false,
			explanation: 'Q general basic 2 explanation 2'
		},
		{
			text: 'Q general basic 2 answer 3 true',
			correct: true,
			explanation: 'Q general basic 2 explanation 3'
		},
		{
			text: 'Q general basic 2 answer 4 false',
			correct: false,
			explanation: 'Q general basic 2 explanation 4'
		}
	]
};

export const QUESTION_GENERAL_MEDIUM_1: Question = {
	id: 3,
	advancement: 'MEDIUM',
	category: 'General',
	text: 'Question general medium 1',
	answers: [
		{
			text: 'Q general medium 1 answer 1 false',
			correct: false,
			explanation: 'Q general medium 1 explanation 1'
		},
		{
			text: 'Q general medium 1 answer 2 false',
			correct: false,
			explanation: 'Q general medium 1 explanation 2'
		},
		{
			text: 'Q general medium 1 answer 3 true',
			correct: true,
			explanation: 'Q general medium 1 explanation 3'
		},
		{
			text: 'Q general medium 1 answer 4 false',
			correct: false,
			explanation: 'Q general medium 1 explanation 4'
		}
	]
};

export const QUESTION_GENERAL_MEDIUM_2: Question = {
	id: 4,
	advancement: 'MEDIUM',
	category: 'General',
	text: 'Question general medium 2',
	answers: [
		{
			text: 'Q general medium 2 answer 1 false',
			correct: false,
			explanation: 'Q general medium 2 explanation 1'
		},
		{
			text: 'Q general medium 2 answer 2 false',
			correct: false,
			explanation: 'Q general medium 2 explanation 2'
		},
		{
			text: 'Q general medium 2 answer 3 true',
			correct: true,
			explanation: 'Q general medium 2 explanation 3'
		},
		{
			text: 'Q general medium 2 answer 4 false',
			correct: false,
			explanation: 'Q general medium 2 explanation 4'
		}
	]
};

export const QUESTION_GENERAL_EXPERT_1: Question = {
	id: 5,
	advancement: 'EXPERT',
	category: 'General',
	text: 'Question general expert 1',
	answers: [
		{
			text: 'Q general expert 1 answer 1 false',
			correct: false,
			explanation: 'Q general expert 1 explanation 1'
		},
		{
			text: 'Q general expert 1 answer 2 false',
			correct: false,
			explanation: 'Q general expert 1 explanation 2'
		},
		{
			text: 'Q general expert 1 answer 3 true',
			correct: true,
			explanation: 'Q general expert 1 explanation 3'
		},
		{
			text: 'Q general expert 1 answer 4 false',
			correct: false,
			explanation: 'Q general expert 1 explanation 4'
		}
	]
};

export const QUESTION_GENERAL_EXPERT_2: Question = {
	id: 6,
	advancement: 'EXPERT',
	category: 'General',
	text: 'Question general expert 2',
	answers: [
		{
			text: 'Q general expert 2 answer 1 false',
			correct: false,
			explanation: 'Q general expert 2 explanation 1'
		},
		{
			text: 'Q general expert 2 answer 2 false',
			correct: false,
			explanation: 'Q general expert 2 explanation 2'
		},
		{
			text: 'Q general expert 2 answer 3 true',
			correct: true,
			explanation: 'Q general expert 2 explanation 3'
		},
		{
			text: 'Q general expert 2 answer 4 false',
			correct: false,
			explanation: 'Q general expert 2 explanation 4'
		}
	]
};

export const QUESTION_JAVA_LANGUAGE_BASIC_1: Question = {
	id: 7,
	advancement: 'BASIC',
	category: 'Java language',
	text: 'Question Java language basic 1',
	answers: [
		{
			text: 'Q Java language basic 1 answer 1 false',
			correct: false,
			explanation: 'Q Java language basic 1 explanation 1'
		},
		{
			text: 'Q Java language basic 1 answer 2 false',
			correct: false,
			explanation: 'Q Java language basic 1 explanation 2'
		},
		{
			text: 'Q Java language basic 1 answer 3 true',
			correct: true,
			explanation: 'Q Java language basic 1 explanation 3'
		},
		{
			text: 'Q Java language basic 1 answer 4 false',
			correct: false,
			explanation: 'Q Java language basic 1 explanation 4'
		}
	]
};

export const QUESTION_JAVA_LANGUAGE_BASIC_2: Question = {
	id: 8,
	advancement: 'BASIC',
	category: 'Java language',
	text: 'Question Java language basic 2',
	answers: [
		{
			text: 'Q Java language basic 2 answer 1 false',
			correct: false,
			explanation: 'Q Java language basic 2 explanation 1'
		},
		{
			text: 'Q Java language basic 2 answer 2 false',
			correct: false,
			explanation: 'Q Java language basic 2 explanation 2'
		},
		{
			text: 'Q Java language basic 2 answer 3 true',
			correct: true,
			explanation: 'Q Java language basic 2 explanation 3'
		},
		{
			text: 'Q Java language basic 2 answer 4 false',
			correct: false,
			explanation: 'Q Java language basic 2 explanation 4'
		}
	]
};

export const QUESTION_JAVA_LANGUAGE_MEDIUM_1: Question = {
	id: 9,
	advancement: 'MEDIUM',
	category: 'Java language',
	text: 'Question Java language medium 1',
	answers: [
		{
			text: 'Q Java language medium 1 answer 1 false',
			correct: false,
			explanation: 'Q Java language medium 1 explanation 1'
		},
		{
			text: 'Q Java language medium 1 answer 2 false',
			correct: false,
			explanation: 'Q Java language medium 1 explanation 2'
		},
		{
			text: 'Q Java language medium 1 answer 3 true',
			correct: true,
			explanation: 'Q Java language medium 1 explanation 3'
		},
		{
			text: 'Q Java language medium 1 answer 4 false',
			correct: false,
			explanation: 'Q Java language medium 1 explanation 4'
		}
	]
};

export const QUESTION_JAVA_LANGUAGE_MEDIUM_2: Question = {
	id: 10,
	advancement: 'MEDIUM',
	category: 'Java Language',
	text: 'Question Java language medium 2',
	answers: [
		{
			text: 'Q Java language medium 2 answer 1 false',
			correct: false,
			explanation: 'Q Java language medium 2 explanation 1'
		},
		{
			text: 'Q Java language medium 2 answer 2 false',
			correct: false,
			explanation: 'Q Java language medium 2 explanation 2'
		},
		{
			text: 'Q Java language medium 2 answer 3 true',
			correct: true,
			explanation: 'Q Java language medium 2 explanation 3'
		},
		{
			text: 'Q Java language medium 2 answer 4 false',
			correct: false,
			explanation: 'Q Java language medium 2 explanation 4'
		}
	]
};

export const QUESTION_JAVA_LANGUAGE_EXPERT_1: Question = {
	id: 11,
	advancement: 'EXPERT',
	category: 'Java Language',
	text: 'Question Java language expert 1',
	answers: [
		{
			text: 'Q Java language expert 1 answer 1 false',
			correct: false,
			explanation: 'Q Java language expert 1 explanation 1'
		},
		{
			text: 'Q Java language expert 1 answer 2 false',
			correct: false,
			explanation: 'Q Java language expert 1 explanation 2'
		},
		{
			text: 'Q Java language expert 1 answer 3 true',
			correct: true,
			explanation: 'Q Java language expert 1 explanation 3'
		},
		{
			text: 'Q Java language expert 1 answer 4 false',
			correct: false,
			explanation: 'Q Java language expert 1 explanation 4'
		}
	]
};

export const QUESTION_JAVA_LANGUAGE_EXPERT_2: Question = {
	id: 12,
	advancement: 'EXPERT',
	category: 'Java Language',
	text: 'Question Java language expert 2',
	answers: [
		{
			text: 'Q Java language expert 2 answer 1 false',
			correct: false,
			explanation: 'Q Java language expert 2 explanation 1'
		},
		{
			text: 'Q Java language expert 2 answer 2 false',
			correct: false,
			explanation: 'Q Java language expert 2 explanation 2'
		},
		{
			text: 'Q Java language expert 2 answer 3 true',
			correct: true,
			explanation: 'Q Java language expert 2 explanation 3'
		},
		{
			text: 'Q Java language expert 2 answer 4 false',
			correct: false,
			explanation: 'Q Java language expert 2 explanation 4'
		}
	]
};

export const QUESTION_SPRING_BASIC_1: Question = {
	id: 13,
	advancement: 'BASIC',
	category: 'Spring',
	text: 'Question Spring basic 1',
	answers: [
		{
			text: 'Q Spring basic 1 answer 1 false',
			correct: false,
			explanation: 'Q Spring basic 1 explanation 1'
		},
		{
			text: 'Q Spring basic 1 answer 2 false',
			correct: false,
			explanation: 'Q Spring basic 1 explanation 2'
		},
		{
			text: 'Q Spring basic 1 answer 3 true',
			correct: true,
			explanation: 'Q Spring basic 1 explanation 3'
		},
		{
			text: 'Q Spring basic 1 answer 4 false',
			correct: false,
			explanation: 'Q Spring basic 1 explanation 4'
		}
	]
};

export const QUESTION_SPRING_BASIC_2: Question = {
	id: 14,
	advancement: 'BASIC',
	category: 'Spring',
	text: 'Question Spring basic 2',
	answers: [
		{
			text: 'Q Spring basic 2 answer 1 false',
			correct: false,
			explanation: 'Q Spring basic 2 explanation 1'
		},
		{
			text: 'Q Spring basic 2 answer 2 false',
			correct: false,
			explanation: 'Q Spring basic 2 explanation 2'
		},
		{
			text: 'Q Spring basic 2 answer 3 true',
			correct: true,
			explanation: 'Q Spring basic 2 explanation 3'
		},
		{
			text: 'Q Spring basic 2 answer 4 false',
			correct: false,
			explanation: 'Q Spring basic 2 explanation 4'
		}
	]
};

export const QUESTION_SPRING_MEDIUM_1: Question = {
	id: 15,
	advancement: 'MEDIUM',
	category: 'Spring',
	text: 'Question Spring medium 1',
	answers: [
		{
			text: 'Q Spring medium 1 answer 1 false',
			correct: false,
			explanation: 'Q Spring medium 1 explanation 1'
		},
		{
			text: 'Q Spring medium 1 answer 2 false',
			correct: false,
			explanation: 'Q Spring medium 1 explanation 2'
		},
		{
			text: 'Q Spring medium 1 answer 3 true',
			correct: true,
			explanation: 'Q Spring medium 1 explanation 3'
		},
		{
			text: 'Q Spring medium 1 answer 4 false',
			correct: false,
			explanation: 'Q Spring medium 1 explanation 4'
		}
	]
};

export const QUESTION_SPRING_MEDIUM_2: Question = {
	id: 16,
	advancement: 'MEDIUM',
	category: 'Spring',
	text: 'Question Spring medium 2',
	answers: [
		{
			text: 'Q Spring medium 2 answer 1 false',
			correct: false,
			explanation: 'Q Spring medium 2 explanation 1'
		},
		{
			text: 'Q Spring medium 2 answer 2 false',
			correct: false,
			explanation: 'Q Spring medium 2 explanation 2'
		},
		{
			text: 'Q Spring medium 2 answer 3 true',
			correct: true,
			explanation: 'Q Spring medium 2 explanation 3'
		},
		{
			text: 'Q Spring medium 2 answer 4 false',
			correct: false,
			explanation: 'Q Spring medium 2 explanation 4'
		}
	]
};

export const QUESTION_SPRING_EXPERT_1: Question = {
	id: 17,
	advancement: 'EXPERT',
	category: 'Spring',
	text: 'Question Spring expert 1',
	answers: [
		{
			text: 'Q Spring expert 1 answer 1 false',
			correct: false,
			explanation: 'Q Spring expert 1 explanation 1'
		},
		{
			text: 'Q Spring expert 1 answer 2 false',
			correct: false,
			explanation: 'Q Spring expert 1 explanation 2'
		},
		{
			text: 'Q Spring expert 1 answer 3 true',
			correct: true,
			explanation: 'Q Spring expert 1 explanation 3'
		},
		{
			text: 'Q Spring expert 1 answer 4 false',
			correct: false,
			explanation: 'Q Spring expert 1 explanation 4'
		}
	]
};

export const QUESTION_SPRING_EXPERT_2: Question = {
	id: 18,
	advancement: 'EXPERT',
	category: 'Spring',
	text: 'Question Spring expert 2',
	answers: [
		{
			text: 'Q Spring expert 2 answer 1 false',
			correct: false,
			explanation: 'Q Spring expert 2 explanation 1'
		},
		{
			text: 'Q Spring expert 2 answer 2 false',
			correct: false,
			explanation: 'Q Spring expert 2 explanation 2'
		},
		{
			text: 'Q Spring expert 2 answer 3 true',
			correct: true,
			explanation: 'Q Spring expert 2 explanation 3'
		},
		{
			text: 'Q Spring expert 2 answer 4 false',
			correct: false,
			explanation: 'Q Spring expert 2 explanation 4'
		}
	]
};

export const QUESTION_MULTI: Question[] = [
	QUESTION_GENERAL_BASIC_1,
	QUESTION_GENERAL_BASIC_2,
	QUESTION_GENERAL_MEDIUM_1,
	QUESTION_GENERAL_MEDIUM_2,
	QUESTION_GENERAL_EXPERT_1,
	QUESTION_GENERAL_EXPERT_2,
	QUESTION_JAVA_LANGUAGE_BASIC_1,
	QUESTION_JAVA_LANGUAGE_BASIC_2,
	QUESTION_JAVA_LANGUAGE_MEDIUM_1,
	QUESTION_JAVA_LANGUAGE_MEDIUM_2,
	QUESTION_JAVA_LANGUAGE_EXPERT_1,
	QUESTION_JAVA_LANGUAGE_EXPERT_2,
	QUESTION_SPRING_BASIC_1,
	QUESTION_SPRING_BASIC_2,
	QUESTION_SPRING_MEDIUM_1,
	QUESTION_SPRING_MEDIUM_2,
	QUESTION_SPRING_EXPERT_1,
	QUESTION_SPRING_EXPERT_2
];
