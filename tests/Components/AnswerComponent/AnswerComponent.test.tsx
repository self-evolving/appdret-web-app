import {QUESTION_GENERAL_BASIC_1} from '../../../tests/__mocks__/MockQuestions';
import {AnswerComponent} from '../../../src/Components/AnswerComponent/AnswerComponent';
import {renderForTest} from '../../__utils__/RenderForTest';
import {fireEvent, screen, waitFor} from '@testing-library/dom';

describe('AnswerComponent', (): void => {
	test('render correct answer', (): void => {
		// arrange
		renderForTest(
			<AnswerComponent
				answer={QUESTION_GENERAL_BASIC_1.answers[3]}
				index={1}
				revealed={true}
				onAnswer={() => undefined}
				onExplanationIconClick={() => undefined}
				explanationIconVisible={true}
			/>
		);
	});

	test('render incorrect answer', (): void => {
		// arrange
		renderForTest(
			<AnswerComponent
				answer={QUESTION_GENERAL_BASIC_1.answers[0]}
				index={1}
				revealed={true}
				onAnswer={() => undefined}
				onExplanationIconClick={() => undefined}
				explanationIconVisible={true}
			/>
		);
	});

	test('click explanation icon', async (): Promise<void> => {
		// arrange
		renderForTest(
			<AnswerComponent
				answer={QUESTION_GENERAL_BASIC_1.answers[0]}
				index={1}
				revealed={true}
				onAnswer={() => undefined}
				onExplanationIconClick={() => undefined}
				explanationIconVisible={true}
			/>
		);

		//act
		const explanationIcon = screen.getAllByTestId('explanationIcon');

		//assert
		fireEvent.click(explanationIcon[0]);
		await waitFor(() =>
			expect(
				screen.getByText('Q general basic 1 explanation 1')
			).toBeInTheDocument()
		);
		fireEvent.click(explanationIcon[0]);
	});

	test('mouse over explanation icon', async (): Promise<void> => {
		// arrange
		renderForTest(
			<AnswerComponent
				answer={QUESTION_GENERAL_BASIC_1.answers[0]}
				index={1}
				revealed={true}
				onAnswer={() => undefined}
				onExplanationIconClick={() => undefined}
				explanationIconVisible={true}
			/>
		);

		//act
		const explanationIcon = screen.getAllByTestId('explanationIcon');

		//assert
		fireEvent.mouseOver(explanationIcon[0]);
		await waitFor(() =>
			expect(
				screen.getByText('Q general basic 1 explanation 1')
			).toBeInTheDocument()
		);
		fireEvent.mouseOut(explanationIcon[0]);
	});
});
