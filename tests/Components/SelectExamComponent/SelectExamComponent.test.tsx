import {mockedGetQuestions} from '../../../tests/__mocks__/MockCommonService';
import {QUESTION_MULTI} from '../../../tests/__mocks__/MockQuestions';
import {SelectExamComponent} from '../../../src/Components/SelectExamComponent/SelectExamComponent';
import {screen} from '@testing-library/react';
import {userEvent} from '@testing-library/user-event';
import '@testing-library/jest-dom';
import {renderForTest} from '../../__utils__/RenderForTest';
import {getQuestions} from '../../../src/Services/QuestionService';

describe('SelectExamComponent', (): void => {
	test('renders correctly without category', async (): Promise<void> => {
		// arrange
		renderForTest(
			<SelectExamComponent
				onSetQuestionsInThePool={() => undefined}
				onSetLevel={() => undefined}
				onSetCategory={() => undefined}
				category={undefined}
			/>
		);

		// act
		const selectedCategory = screen.getByTestId('category-select');
		const selectedLevel = screen.getByTestId('level-select');

		// assert
		expect(selectedCategory).toBeInTheDocument();
		expect(selectedLevel).toBeInTheDocument();
	});

	test('selecting level and category works properly', async (): Promise<void> => {
		// arrange
		const user = userEvent.setup();
		mockedGetQuestions.mockReturnValue(QUESTION_MULTI);
		renderForTest(
			<SelectExamComponent
				onSetQuestionsInThePool={() => undefined}
				onSetLevel={() => undefined}
				onSetCategory={() => undefined}
				category='General'
			/>
		);

		// act
		const questions = await getQuestions();
		const selectedCategory = screen.getByTestId('category-select');
		const selectedLevel = screen.getByTestId('level-select');
		const selectedQuestionPool = screen.getByText('exam');
		await user.selectOptions(selectedCategory, ['undefined']);
		await user.selectOptions(selectedLevel, ['undefined']);
		await user.click(selectedQuestionPool);

		// assert
		expect(questions).toStrictEqual(QUESTION_MULTI);
		expect(selectedQuestionPool).toBeInTheDocument();
		expect(selectedCategory).toHaveValue('undefined');
		expect(selectedLevel).toHaveValue('undefined');
	});
});
