import {QUESTION_GENERAL_BASIC_1} from '../../__mocks__/MockQuestions';
import {QuestionComponent} from '../../../src/Components/QuestionComponent/QuestionComponent';
import {fireEvent, screen} from '@testing-library/react';
import '@testing-library/jest-dom';
import {renderForTest} from '../../__utils__/RenderForTest';

describe('QuestionComponent', (): void => {
	test('renders correctly', (): void => {
		// arrange
		renderForTest(
			<QuestionComponent
				question={QUESTION_GENERAL_BASIC_1}
				selectedAnswer={1}
				onAnswer={() => undefined}
				onExplanationIconClick={() => undefined}
				explanationIconVisible={true}
			/>
		);

		// act & assert
		expect(
			screen.getByText('Question general basic 1')
		).toBeInTheDocument();
	});

	test('handle correct answer', (): void => {
		// arrange
		renderForTest(
			<QuestionComponent
				question={QUESTION_GENERAL_BASIC_1}
				selectedAnswer={2}
				onAnswer={() => undefined}
				onExplanationIconClick={() => undefined}
				explanationIconVisible={true}
			/>
		);

		// act
		const answerBtn = screen.getByText('Q general basic 1 answer 3 true');

		// assert
		fireEvent.click(answerBtn);
	});

	test('handle incorrect answer', (): void => {
		// arrange
		renderForTest(
			<QuestionComponent
				question={QUESTION_GENERAL_BASIC_1}
				selectedAnswer={undefined}
				onAnswer={() => undefined}
				onExplanationIconClick={() => undefined}
				explanationIconVisible={true}
			/>
		);

		// act
		const answerBtn = screen.getByText('Q general basic 1 answer 4 false');

		// assert
		fireEvent.click(answerBtn);
	});
});
