import {mockedUseNavigate} from '../../../tests/__mocks__/MockCommonService';
import {FinalResultComponent} from '../../../src/Components/FinalResultComponent/FinalResultComponent';
import {screen, waitFor} from '@testing-library/react';
import '@testing-library/jest-dom';
import {userEvent} from '@testing-library/user-event';
import {renderForTest} from '../../__utils__/RenderForTest';

describe('FinalResultComponent', (): void => {
	test('handle button click', async (): Promise<void> => {
		// arrange
		const user = userEvent.setup();
		renderForTest(<FinalResultComponent />);

		// act
		const tryAgainBtn = screen.getByText('try again');

		// assert
		expect(screen.getByText('Your points')).toBeInTheDocument();
		expect(tryAgainBtn).toBeInTheDocument();
		await user.click(tryAgainBtn);
		await waitFor((): void => {
			expect(mockedUseNavigate).toHaveBeenCalledWith('/');
		});
	});
});
