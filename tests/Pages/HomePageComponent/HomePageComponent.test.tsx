import {HomePageComponent} from '../../../src/Pages/HomePageComponent/HomePageComponent';
import '@testing-library/jest-dom';
import {renderForTest} from '../../__utils__/RenderForTest';
import {screen} from '@testing-library/dom';
import {userEvent} from '@testing-library/user-event';

describe('HomePageComponent', (): void => {
	test('renders correctly and button works properly', async (): Promise<void> => {
		// arrange
		const user = userEvent.setup();
		renderForTest(
			<HomePageComponent
				onSetQuestionsInThePool={() => undefined}
				onSetLevel={() => undefined}
				onSetCategory={() => undefined}
				category={undefined}
				level={undefined}
			/>
		);

		// act
		const doitBtn = screen.getByText('do it!');

		// assert
		expect(doitBtn).toBeInTheDocument();
		await user.click(doitBtn);
	});
});
