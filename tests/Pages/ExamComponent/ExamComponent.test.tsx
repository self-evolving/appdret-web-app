import {QUESTION_MULTI} from '../../../tests/__mocks__/MockQuestions';
import {mockedGetQuestions} from '../../../tests/__mocks__/MockCommonService';
import {ExamComponent} from '../../../src/Pages/ExamComponent/ExamComponent';
import '@testing-library/jest-dom';
import {renderForTest} from '../../__utils__/RenderForTest';
import {screen, waitFor} from '@testing-library/dom';
import {userEvent} from '@testing-library/user-event';

describe('ExamComponent', (): void => {
	test('renders correctly and save button works properly', async (): Promise<void> => {
		// arrange
		const user = userEvent.setup();
		mockedGetQuestions.mockReturnValue(QUESTION_MULTI);
		renderForTest(
			<ExamComponent
				level='MEDIUM'
				category={undefined}
				questionsInThePool={5}
			/>
		);

		await waitFor(async () => {
			// act
			const saveBtn = screen.getByText('save');
			const answer = screen.getAllByTestId('answer-select');

			// assert
			expect(saveBtn).toBeInTheDocument();
			await user.click(answer[0]);
			await user.click(saveBtn);
		});
	});

	test('next button works properly', async (): Promise<void> => {
		// arrange
		const user = userEvent.setup();
		mockedGetQuestions.mockReturnValue(QUESTION_MULTI);
		renderForTest(
			<ExamComponent
				level='MEDIUM'
				category='Spring'
				questionsInThePool={2}
			/>
		);

		await waitFor(async () => {
			// act
			const nextBtn = screen.getByText('next');
			const answer = screen.getAllByTestId('answer-select');

			// assert
			expect(nextBtn).toBeInTheDocument();
			await user.click(answer[2]);
			await user.click(nextBtn);
		});
	});
});
