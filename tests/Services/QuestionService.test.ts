import {QUESTION_MULTI} from '../__mocks__/MockQuestions';
import MockAdapter from 'axios-mock-adapter';
import axios, {AxiosError} from 'axios';
import {getQuestions} from '../../src/Services/QuestionService';

describe('QuestionService', (): void => {
	test('get questions', async (): Promise<void> => {
		// arrange
		const mock = new MockAdapter(axios);
		mock.onGet('https://public.andret.eu/questions.json').reply(
			200,
			QUESTION_MULTI
		);

		//act
		const questions = await getQuestions();

		// assert
		expect(questions).toStrictEqual(QUESTION_MULTI);
	});

	test('get questions rejected', (): void => {
		// arrange
		const mock = new MockAdapter(axios);
		mock.onGet('https://public.andret.eu/questions.json').reply(500);

		//act
		getQuestions().catch((error: AxiosError): void => {
			// assert
			expect(error.message).toEqual(
				'Request failed with status code 500'
			);
		});
	});
});
