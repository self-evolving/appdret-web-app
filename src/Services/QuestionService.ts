import axios, {isAxiosError} from 'axios';
import {Question} from '../Entities/Question';

const backendUrl: string = 'https://public.andret.eu/questions.json';

export const getQuestions = async (): Promise<Question[]> => {
	try {
		const result = await axios.get<Question[]>(backendUrl, {
			headers: {
				Accept: 'application/json',
				'Content-Type': 'application/json'
			}
		});
		return result.data;
	} catch (err) {
		if (isAxiosError(err)) {
			console.error(err.response);
		}
		console.error(err);
		return [];
	}
};
