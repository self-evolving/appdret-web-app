import React from 'react';
import './_ExamComponent.scss';
import {QuestionComponent} from '../../Components/QuestionComponent/QuestionComponent';
import {Question} from '../../Entities/Question';
import {getQuestions} from '../../Services/QuestionService';
import {useNavigate} from 'react-router-dom';

export interface ExamComponentProps {
	readonly level?: string;
	readonly category?: string;
	readonly questionsInThePool?: number;
}

export const ExamComponent = (
	props: ExamComponentProps
): React.ReactElement => {
	const [questions, setQuestions] = React.useState<Question[]>([]);
	const [questionNumber, setQuestionNumber] = React.useState<number>(0);
	const [selectedAnswer, setSelectedAnswer] = React.useState<
		number | undefined
	>(undefined);
	const [points, setPoints] = React.useState<number>(0);
	const [explanationIconVisible, setExplanationIconVisible] =
		React.useState<boolean>(false);
	const [isLoaded, setIsLoaded] = React.useState<boolean>(false);

	const navigate = useNavigate();

	React.useEffect((): void => {
		fetchAndSetQuestion();
	}, []);

	const fetchAndSetQuestion = async (): Promise<void> => {
		const allQuestions = await getQuestions();
		const questionPool = setNewQuestionPool(
			allQuestions.toSorted(() => 0.5 - Math.random())
		);
		setQuestions([...questionPool]);
		setIsLoaded(true);
	};

	const setNewQuestionPool = (allQuestions: Question[]) => {
		if (props.category) {
			allQuestions = allQuestions.filter(
				(item) => item.category === props.category
			);
		}
		if (props.level) {
			allQuestions = allQuestions.filter(
				(item) => item.advancement === props.level
			);
		}
		if (!props.category) {
			allQuestions = allQuestions.slice(0, props.questionsInThePool);
		}
		return allQuestions.slice(0, props.questionsInThePool);
	};

	const handleNextBtn = (): void => {
		setQuestionNumber(
			(questionNumber: number): number => questionNumber + 1
		);
		setSelectedAnswer(undefined);
		setExplanationIconVisible(false);
	};

	const handleAnswer = (selectedAnswer: number): void => {
		setSelectedAnswer(selectedAnswer);
		if (questions[questionNumber].answers[selectedAnswer].correct) {
			return setPoints((points: number): number => points + 1);
		}
	};

	return (
		<div className='exam'>
			<div className='exam__header'>
				<h1>Appdret</h1>
				<p>{isLoaded && `${questionNumber + 1}/${questions.length}`}</p>
			</div>
			<div className='exam__questions'>
				{isLoaded ? (
					<QuestionComponent
						question={questions[questionNumber]}
						selectedAnswer={selectedAnswer}
						onAnswer={handleAnswer}
						onExplanationIconClick={setExplanationIconVisible}
						explanationIconVisible={explanationIconVisible}
					/>
				) : (
					<p>Loading questions...</p>
				)}
			</div>
			<div className='question__buttons'>
				<button
					className='btn question__buttons--btn'
					onClick={() => {
						navigate('/final', {
							state: {
								pointsScored: points,
								numberOfQuestions: questions.length,
								open: true
							}
						});
					}}
				>
					save
				</button>

				<button
					disabled={
						selectedAnswer === undefined ||
						questionNumber + 1 === questions.length
					}
					className='btn question__buttons--btn'
					onClick={handleNextBtn}
				>
					next
				</button>
			</div>
		</div>
	);
};
