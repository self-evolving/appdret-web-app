import {useNavigate} from 'react-router-dom';
import React from 'react';
import {SelectExamComponent} from '../../Components/SelectExamComponent/SelectExamComponent';
import './_HomePageComponent.scss';

export interface HomePageComponentProps {
	readonly level?: string;
	readonly category?: string;
	readonly onSetLevel: (level?: string) => void;
	readonly onSetQuestionsInThePool: (numberOfQuestions?: number) => void;
	readonly onSetCategory: (category?: string) => void;
}

export const HomePageComponent = (
	props: HomePageComponentProps
): React.ReactElement => {
	const navigate = useNavigate();
	return (
		<div
			className='homepage'
			data-aos='fade-up'
		>
			<h1>Appdret</h1>
			<p>
				Check your <span className='homepage__java'>java</span>{' '}
				knowledge
			</p>
			<SelectExamComponent
				category={props.category}
				onSetQuestionsInThePool={props.onSetQuestionsInThePool}
				onSetCategory={props.onSetCategory}
				onSetLevel={props.onSetLevel}
			/>
			<button
				className='btn'
				onClick={(): void => navigate('/exam')}
			>
				do it!
			</button>
		</div>
	);
};
