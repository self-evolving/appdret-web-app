import {createRoot} from 'react-dom/client';
import React from 'react';
import {BrowserRouter} from 'react-router-dom';
import AOS from 'aos';
import {App} from './App';

const container = document.getElementById('root') as Element;
const root = createRoot(container);
AOS.init();

root.render(
	<React.StrictMode>
		<BrowserRouter>
			<App />
		</BrowserRouter>
	</React.StrictMode>
);
