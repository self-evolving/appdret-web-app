import React from 'react';
import {Routes, Route} from 'react-router-dom';
import {HomePageComponent} from './Pages/HomePageComponent/HomePageComponent';
import {ExamComponent} from './Pages/ExamComponent/ExamComponent';
import './styles.scss';
import 'aos/dist/aos.css';
import {FinalResultComponent} from './Components/FinalResultComponent/FinalResultComponent';

export const App = (): React.ReactElement => {
	const [category, setCategory] = React.useState<string | undefined>(
		undefined
	);
	const [level, setLevel] = React.useState<string | undefined>(undefined);
	const [questionsInThePool, setQuestionsInThePool] = React.useState<
		number | undefined
	>(undefined);

	return (
		<Routes>
			<Route
				path={'/'}
				element={
					<HomePageComponent
						level={level}
						category={category}
						onSetQuestionsInThePool={setQuestionsInThePool}
						onSetCategory={setCategory}
						onSetLevel={setLevel}
					/>
				}
			/>
			<Route
				path={'exam'}
				element={
					<ExamComponent
						level={level}
						category={category}
						questionsInThePool={questionsInThePool}
					/>
				}
			/>
			<Route
				path={'final'}
				element={<FinalResultComponent />}
			/>
		</Routes>
	);
};
