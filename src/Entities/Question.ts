export interface Question {
	readonly id: number;
	readonly text: string;
	readonly category: string;
	readonly advancement: string;
	readonly answers: Answer[];
}

export interface Answer {
	readonly text: string;
	readonly correct: boolean;
	readonly explanation: string;
}
