import React from 'react';
import {Question} from '../../Entities/Question';
import './_QuestionComponent.scss';
import {AnswerComponent} from '../AnswerComponent/AnswerComponent';

interface QuestionComponentProps {
	readonly question: Question;
	readonly selectedAnswer: number | undefined;
	readonly explanationIconVisible: boolean;
	readonly onExplanationIconClick: (explanationIconVisible: boolean) => void;
	readonly onAnswer: (answerId: number) => void;
}

export const QuestionComponent = (
	props: QuestionComponentProps
): React.ReactElement => {
	const handleAnswerClick = (index: number) => {
		if (props.selectedAnswer === undefined) {
			props.onAnswer(index);
		}
	};

	return (
		<div className='question'>
			<div className='question__heading'>
				<pre className='question__active-question'>
					{props.question.text}
				</pre>
			</div>
			<div className='answers'>
				{props.question.answers.map((answer, index) => (
					<AnswerComponent
						answer={answer}
						key={index}
						index={index}
						revealed={props.selectedAnswer === index}
						explanationIconVisible={props.explanationIconVisible}
						onExplanationIconClick={props.onExplanationIconClick}
						onAnswer={() => handleAnswerClick(index)}
					/>
				))}
			</div>
		</div>
	);
};
