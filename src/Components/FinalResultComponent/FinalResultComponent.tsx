import React from 'react';
import './_FinalResultComponent.scss';
import {useLocation, useNavigate} from 'react-router-dom';

export const FinalResultComponent = (): React.ReactElement => {
	const navigate = useNavigate();
	const location = useLocation();
	const pointsScored = location?.state?.pointsScored;
	const numberOfQuestions = location?.state?.numberOfQuestions;
	const renderPercentageOfScore = (): string =>
		`${((pointsScored / numberOfQuestions) * 100).toFixed(2)}%`;

	return (
		<div className='final_result'>
			<h2 data-aos='fade-down'>Appdret</h2>
			<div className='final_result__points'>
				<p>
					Your points&nbsp;
					<span className='final_result__countedPoints'>
						{pointsScored}/{numberOfQuestions}
					</span>
				</p>
				<p>{renderPercentageOfScore()}</p>
			</div>
			<button
				data-aos='fade-up'
				className='btn'
				onClick={() => navigate('/')}
			>
				try again
			</button>
		</div>
	);
};
