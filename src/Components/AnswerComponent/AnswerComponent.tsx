import React from 'react';
import './_AnswerComponent.scss';
import {Answer} from '../../Entities/Question';
import {Info} from 'react-feather';
import {Tooltip, Typography} from '@mui/material';

export interface AnswerComponentProps {
	readonly answer: Answer;
	readonly index: number;
	readonly revealed: boolean;
	readonly explanationIconVisible: boolean;
	readonly onExplanationIconClick: (explanationIconVisible: boolean) => void;
	readonly onAnswer: () => void;
}

export const AnswerComponent = (
	props: AnswerComponentProps
): React.ReactElement => {
	const [explanationVisibility, setExplanationVisibility] =
		React.useState<boolean>(false);

	React.useEffect(() => {
		setExplanationVisibility(false);
	}, [props.answer]);

	const handleAnswerCorrectness = (): string => {
		if (props.answer.correct) {
			return 'answers__answerBtn--correct';
		}
		return 'answers__answerBtn--incorrect';
	};

	const handleAnswerClick = (): void => {
		props.onAnswer();
		props.onExplanationIconClick(true);
	};

	return (
		<div className='answers__answer'>
			<button
				className={`answers__answerBtn ${props.revealed && handleAnswerCorrectness()}`}
				disabled={props.revealed}
				value={props.index}
				onClick={handleAnswerClick}
				data-testid='answer-select'
			>
				<pre className='answers__answer--text'>{props.answer.text}</pre>
			</button>

			{props.explanationIconVisible && (
				<Tooltip
					sx={{size: '1.5rem'}}
					title={
						<Typography fontSize={'1.5rem'}>
							{props.answer.explanation}
						</Typography>
					}
					enterDelay={100}
					leaveDelay={100}
					open={explanationVisibility}
				>
					<Info
						className='answers__answer--explanationIcon'
						data-testid='explanationIcon'
						onClick={() =>
							setExplanationVisibility(!explanationVisibility)
						}
						onMouseOver={() => setExplanationVisibility(true)}
						onMouseLeave={() => setExplanationVisibility(false)}
					/>
				</Tooltip>
			)}
		</div>
	);
};
