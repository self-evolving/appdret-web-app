import React from 'react';
import './_SelectExamComponent.scss';
import {Question} from '../../Entities/Question';
import {getQuestions} from '../../Services/QuestionService';

export interface SelectExamComponentProps {
	readonly category?: string;
	readonly onSetQuestionsInThePool: (numberOfQuestions?: number) => void;
	readonly onSetLevel: (level?: string) => void;
	readonly onSetCategory: (category?: string) => void;
}

export const SelectExamComponent = (
	props: SelectExamComponentProps
): React.ReactElement => {
	const [questions, setQuestions] = React.useState<Question[]>([]);
	const [uniqueQuestions, setUniqueQuestions] =
		React.useState<Question[]>(questions);
	const [levels, setLevels] = React.useState<string[]>([]);
	const [categories, setCategories] = React.useState<string[]>([]);

	React.useEffect((): void => {
		fetchData();
	}, []);

	React.useEffect((): void => {
		selectAndSetUniqueQuestions();
	}, [props.category, questions]);

	React.useEffect((): void => {
		setUniqueLevels();
	}, [uniqueQuestions]);

	React.useEffect((): void => {
		getAndSetCategories();
	}, [questions]);

	const fetchData = async (): Promise<void> => {
		await fetchQuestions();
	};

	const fetchQuestions = async (): Promise<void> => {
		const fetchedQuestions = await getQuestions();
		setQuestions(fetchedQuestions);
	};

	const handleChangeLevel = (
		e: React.ChangeEvent<HTMLSelectElement>
	): void => {
		props.onSetLevel(
			e.target.value === 'undefined' ? undefined : e.target.value
		);
	};

	const handleChangeCategory = (
		e: React.ChangeEvent<HTMLSelectElement>
	): void => {
		props.onSetCategory(
			e.target.value === 'undefined' ? undefined : e.target.value
		);
	};

	const handleChangeAmountOfQuestions = (
		e: React.MouseEvent<HTMLInputElement>
	): void => {
		props.onSetQuestionsInThePool(+e.currentTarget.value);
	};

	const getAndSetCategories = (): void => {
		const allCategories = questions.map((question) => question.category);
		const uniqueCategories = Array.from(new Set(allCategories));
		return setCategories(uniqueCategories);
	};

	const setUniqueLevels = (): void => {
		const allAdvancementLevels = uniqueQuestions.map(
			(question) => question.advancement
		);
		const uniqueAdvancementLevels = Array.from(
			new Set(allAdvancementLevels)
		);
		return setLevels([...uniqueAdvancementLevels]);
	};

	const selectAndSetUniqueQuestions = (): void => {
		if (!props.category) {
			return setUniqueQuestions([...questions]);
		}
		const filteredQuestions = questions.filter(
			(question: Question) => question.category === props.category
		);
		return setUniqueQuestions([...filteredQuestions]);
	};

	const renderCategory = (category: string): React.ReactElement => {
		return (
			<option
				value={category}
				className='select-exam__select-module--option'
			>
				{category}
			</option>
		);
	};

	const renderLevel = (level: string): React.ReactElement => {
		return (
			<option
				value={level}
				className='select-exam__select-module--option'
			>
				{level}
			</option>
		);
	};

	return (
		<div className='select-exam'>
			<div className='select-exam__select-module'>
				<h2>
					select <span className='main-yellow'>category</span>
				</h2>
				<select
					name='category'
					id='category'
					data-testid='category-select'
					className='select-exam__select-module--select'
					onChange={handleChangeCategory}
				>
					<option
						value='undefined'
						selected
						className='select-exam__select-module--option'
					>
						All categories
					</option>
					{categories.map(renderCategory)}
				</select>
			</div>
			<div className='select-exam__select-module'>
				<h2>
					select <span className='main-yellow'>level</span>
				</h2>
				<select
					name='level'
					id='level'
					data-testid='level-select'
					className='select-exam__select-module--select'
					onChange={handleChangeLevel}
				>
					<option
						className='select-exam__select-module--option'
						selected
						value='undefined'
					>
						ALL LEVELS
					</option>

					{levels.map(renderLevel)}
				</select>
			</div>

			<div className='select-exam__questions-pool'>
				<label>
					<input
						type='radio'
						name='pool'
						value='15'
						onClick={handleChangeAmountOfQuestions}
					/>
					&nbsp;warmup
				</label>
				<label>
					<input
						type='radio'
						name='pool'
						value='30'
						onClick={handleChangeAmountOfQuestions}
					/>
					&nbsp;exam
				</label>
				<label>
					<input
						type='radio'
						name='pool'
						value='60'
						onClick={handleChangeAmountOfQuestions}
					/>
					&nbsp;hardcore
				</label>
			</div>
		</div>
	);
};
