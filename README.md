## Open project dir

cd appdret-web-app

## Install dependencies

yarn

## Run dev server

yarn start

## Build

yarn build
